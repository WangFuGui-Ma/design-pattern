package observer.jdk;


public class Client {

    public static void main(String[] args) {

        WeatherData weatherData = new WeatherData();
        //创建观察者
        CurrentConditions currentConditions = new CurrentConditions();
        BaiduSite baiduSite = new BaiduSite();

        //注册到weatherData
        weatherData.addObserver(currentConditions);
        weatherData.addObserver(baiduSite);

        weatherData.setData(63,51,66);
        System.out.println("---------------------------变天了------------------------");
        weatherData.setData(6,43,6);
    }

}
