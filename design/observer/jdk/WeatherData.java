package observer.jdk;


import java.util.Observable;

public class WeatherData extends Observable {

	private float temperature;
	private float pressure;
	private float humidity;

	//当数据有更新时，就调用 setData
	public void setData(float temperature, float pressure, float humidity) {
		this.temperature = temperature;
		this.pressure = pressure;
		this.humidity = humidity;
		//notifyObservers， 将最新的信息 推送给 接入方 currentConditions
		setChanged();
		notifyObservers(this);
	}

	public float getTemperature() {
		return temperature;
	}

	public float getPressure() {
		return pressure;
	}

	public float getHumidity() {
		return humidity;
	}
}
