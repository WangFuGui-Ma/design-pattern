package observer.jdk;


import java.util.Observable;
import java.util.Observer;

public class CurrentConditions implements Observer {

	// �¶ȣ���ѹ��ʪ��
	private float temperature;
	private float pressure;
	private float humidity;


	// ��ʾ
	public void display() {
		System.out.println("***Today mTemperature: " + temperature + "***");
		System.out.println("***Today mPressure: " + pressure + "***");
		System.out.println("***Today mHumidity: " + humidity + "***");
	}

	/**
	 * This method is called whenever the observed object is changed. An
	 * application calls an <tt>Observable</tt> object's
	 * <code>notifyObservers</code> method to have all the object's
	 * observers notified of the change.
	 *
	 * @param o   the observable object.
	 * @param arg an argument passed to the <code>notifyObservers</code>
	 */
	@Override
	public void update(Observable o, Object arg) {
		WeatherData weatherData = (WeatherData) arg;
		this.temperature = weatherData.getTemperature();
		this.pressure = weatherData.getPressure();
		this.humidity = weatherData.getHumidity();
		display();
	}
}
