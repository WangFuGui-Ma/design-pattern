package observer.jdk;


import java.util.Observable;
import java.util.Observer;

public class BaiduSite implements Observer {

	// 温度，气压，湿度
	private float temperature;
	private float pressure;
	private float humidity;

	@Override
	public void update(Observable o, Object arg) {
		WeatherData weatherData = (WeatherData) arg;
		this.temperature = weatherData.getTemperature();
		this.pressure = weatherData.getPressure();
		this.humidity = weatherData.getHumidity();
		display();
	}
	// 显示
	public void display() {
		System.out.println("===百度网站====");
		System.out.println("***百度网站 气温 : " + temperature + "***");
		System.out.println("***百度网站 气压: " + pressure + "***");
		System.out.println("***百度网站 湿度: " + humidity + "***");
	}

}
