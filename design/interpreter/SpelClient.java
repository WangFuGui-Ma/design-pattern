package interpreter;

import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * @Description TODO
 * @Author masiyi
 * @Date 2022/12/15
 **/
public class SpelClient {
    public static void main(String[] args) {

        SpelExpressionParser spelExpressionParser = new SpelExpressionParser();
        Expression expression = spelExpressionParser.parseExpression("10 + 20 - 25");
        System.out.println(expression.getValue());
    }
}
