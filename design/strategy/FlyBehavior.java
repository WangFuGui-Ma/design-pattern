package strategy;

/**
 * @author 飞翔行为接口
 */
public interface FlyBehavior {
	
	void fly(); // 子类具体实现
}
