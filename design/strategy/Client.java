package strategy;

public class Client {

	public static void main(String[] args) {
		WildDuck wildDuck = new WildDuck();
		System.out.println("野鸭的飞翔能力");
		wildDuck.fly();
		wildDuck.quack();

		ToyDuck toyDuck = new ToyDuck();
		System.out.println("玩具鸭的飞翔能力");
		toyDuck.fly();
		toyDuck.quack();

		PekingDuck pekingDuck = new PekingDuck();
		System.out.println("北京鸭的飞翔能力");
		pekingDuck.fly();
		pekingDuck.quack();

		//动态改变某个对象的行为, 北京鸭 不能飞
		pekingDuck.setFlyBehavior(new NoFlyBehavior());
		System.out.println("北京鸭的实际飞翔能力");
		pekingDuck.fly();
	}

}
