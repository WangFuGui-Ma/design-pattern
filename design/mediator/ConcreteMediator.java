package mediator;

import java.util.HashMap;

//具体的中介者类
public class ConcreteMediator extends Mediator {
	//集合，放入所有的同事对象
	private final HashMap<String, Colleague> colleagueMap =  new HashMap<>();

	@Override
	public void Register(String colleagueName, Colleague colleague) {
		colleagueMap.put(colleagueName, colleague);
	}

	//具体中介者的核心方法
	//1. 根据得到消息，完成对应任务
	//2. 中介者在这个方法，协调各个具体的同事对象，完成任务
	@Override
	public void GetMessage(Integer stateChange, String colleagueName) {

		//处理闹钟发出的消息
		if (colleagueMap.get(colleagueName) instanceof Alarm) {
			//开
			if (stateChange == 0) {
				((CoffeeMachine) (colleagueMap.get("coffeeMachine"))).StartCoffee();
			//关
			} else if (stateChange == 1) {
				((TV) (colleagueMap.get("TV"))).StopTv();
			}

		} else if (colleagueMap.get(colleagueName) instanceof CoffeeMachine) {
			((Curtains) (colleagueMap.get("curtains"))).UpCurtains();

		} else if (colleagueMap.get(colleagueName) instanceof TV) {
			//如果TV发现消息
		} else if (colleagueMap.get(colleagueName) instanceof Curtains) {
			((TV) (colleagueMap.get("TV"))).StartTv();
		}

	}

	@Override
	public void sendMessage() {
		// TODO Auto-generated method stub

	}

}
