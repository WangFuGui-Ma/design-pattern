package mediator;

/**
 * 咖啡机
 */
public class CoffeeMachine extends Colleague {

	public CoffeeMachine(Mediator mediator, String name) {
		super(mediator, name);
		// TODO Auto-generated constructor stub
		mediator.Register(name, this);
	}

	@Override
	public void sendMessage(Integer stateChange) {
		// TODO Auto-generated method stub
		this.getMediator().GetMessage(stateChange, this.name);
	}

	public void StartCoffee() {
		System.out.println("开始制作咖啡了!");
	}

	public void FinishCoffee() {

		System.out.println("5分钟后!");
		System.out.println("咖啡 is ok!");
		sendMessage(null);
	}
}
