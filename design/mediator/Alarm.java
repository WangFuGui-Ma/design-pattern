package mediator;

/**
 * 闹钟 具体的同事类
 */
public class Alarm extends Colleague {

	//构造器
	public Alarm(Mediator mediator, String name) {
		super(mediator, name);
		// TODO Auto-generated constructor stub
		//在创建Alarm 同事对象时，将自己放入到ConcreteMediator 对象中[集合]
		mediator.Register(name, this);
	}

	public void sendAlarm(int stateChange) {
		sendMessage(stateChange);
	}

	@Override
	public void sendMessage(Integer stateChange) {
		// TODO Auto-generated method stub
		//调用的中介者对象的getMessage
		this.getMediator().GetMessage(stateChange, this.name);
	}

}
