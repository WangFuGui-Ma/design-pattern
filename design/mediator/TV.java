package mediator;

public class TV extends Colleague {

	public TV(Mediator mediator, String name) {
		super(mediator, name);
		mediator.Register(name, this);
	}

	@Override
	public void sendMessage(Integer stateChange) {
		this.getMediator().GetMessage(stateChange, this.name);
	}

	public void StartTv() {
		System.out.println("是时候开始开电视了!");
	}

	public void StopTv() {
		System.out.println("停止电视!");
	}
}
